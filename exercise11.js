let model = (operation, obj, schema) =>{
    var DB = [];
    var newObj = {};

    if(operation == 'add'){
        for (var key in obj){
            for (var key2 in schema){
                if(key == key2 && typeof obj[key] == schema[key2]){ // checking if the key and typeof the value is the same as in schema  
                    newObj[key] = obj[key] 
                }
            }
        }
    }
    DB.push(newObj)
    return DB
}

module.exports = {
    model
}
