let model = (operation, obj, schema) =>{
    var DB = [];

    if(operation == 'add'){
		var newObj = {};
		for (let key in schema){  
			if (key in obj && typeof obj[key] == schema[key].type){
				newObj[key] = obj[key];
			}
			else if('default' in schema[key]) {
				newObj[key] = schema[key].default
			}
		}
    }
    DB.push(newObj)
    return DB
}


module.exports = {
	model
}