let bankAccount = {
    total: 0,
    withdraw: function(sub) {bankAccount.total -= sub},
    deposit: function(amountAdded) {bankAccount.total +=amountAdded},
    balance: function() {return bankAccount.total}
}

module.exports = {
    bankAccount
}