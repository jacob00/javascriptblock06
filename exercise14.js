let splice = (obj, start, length) =>{
	let array = Object.entries(obj);
	if(length == undefined){
		length = 1
	}

	array = array.splice(start,length)
	
	return Object.fromEntries(array);
}
module.exports ={
	splice
}