let sort = (obj) =>{
    let array = []
    for (var key in obj){
        array.push([key,obj[key]])
    }
    array.sort(function(a, b) {
        return a[1] - b[1];
    });

    let newObj={}
    for(var i=0; i<array.length; i++){
        newObj[array[i][0]] = array[i][1]
    }
    
    return newObj
}
module.exports ={
    sort
}
