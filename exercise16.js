let sort = (obj, type, order) =>{
    let array = []
    for (var key in obj){
        array.push([key,obj[key]])
    }
    if(type == 'values' && order == 'ascending'){
        array.sort(function(a, b) {
            return a[1] - b[1];
        });
    }
    else if(type == 'values' && order == 'descending'){
        array.sort(function(a, b) {
            return b[1] - a[1];
        });
    }
    else if(type == 'keys' && order == 'ascending'){
        array.sort();
    }
    else if(type == 'keys' && order == 'descending'){
        array.sort();
        array.reverse();
    }

    let newObj={}
    for(var i=0; i<array.length; i++){
        newObj[array[i][0]] = array[i][1]
    }
    
    return newObj
}
module.exports ={
    sort
}