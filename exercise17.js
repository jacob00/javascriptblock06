let biggestNumber = (obj) =>{
    let array = []
    for (var key in obj){
        array.push([key,obj[key]])
    }
    array.sort(function(a, b) {
        return b[1] - a[1];
    });


    let newObj={
        num: array[0][1] ,
        key: array[0][0]
    }
    return newObj
}
module.exports ={
    biggestNumber
}