let addToList = (movies) => {
    var movieList = []
    movies.forEach((element,index) => {
        let obj = {}
        obj.title = element
        obj.id = index
        movieList.push(obj)
    });
    return movieList
}
module.exports = {
    addToList
}