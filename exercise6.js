let getIndex = (array, key, value) =>{
    let index = -1
    for(var i = 0; i<array.length; i++){
        if(array[i][key] == value){
            index = i;
        }
    }
    return index
}

module.exports ={
    getIndex
}