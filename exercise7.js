let runOnRange = (obj) => {
    let array = [];
    let {start, end, step} = obj; 

    if(step == undefined || step == 0){
        return array
    }

    if(start > end && step < 1){
        for(var i = start; i >= end; i+=step){
            array.push(i)
        }
    }
    else {
        for (var x = start; x <= end; x+=step){
            array.push(x)
        }
    }
    return array
}

module.exports = {
    runOnRange
}
