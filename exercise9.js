let sumAll = (obj) => {
    let array = [];
    let sum = 0;
    if (obj == undefined){
        return 0
    } 
    else {
        array = Object.values(obj)
        for (var i = 0; i < array.length; i++){
            sum += array[i]
        }
    }
    return sum
}
module.exports = {
    sumAll
}